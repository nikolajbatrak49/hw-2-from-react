
import { Component } from "react";

import { Modal } from "./components/modal";
import ProductList from "./components/product-list";

import styles from "./styles/app.module.scss";
import favoriteIcon from "./img/favourites-star.png";
import basketIcon from "./img/basket.png";


class App extends Component {
  constructor() {
    super();
    this.state = {
      products: [],
      productsInFavorite: [],
      productsInBasket: [],
      modal: {
        visible: false, 
        modalId: null, 
        submitFunction: null,
      },
    };
  }

  componentDidMount() {
    fetch("productCollection.json")
      .then((response) => response.json())
      .then((products) => this.setState({ products: products }));
      
    localStorage.getItem("productsInFavorite")
      ? this.setState({
          productsInFavorite: JSON.parse(
            localStorage.getItem("productsInFavorite")
          ),
        })
      : localStorage.setItem("productsInFavorite", JSON.stringify([]));

    localStorage.getItem("productsInBasket")
      ? this.setState({
          productsInBasket: JSON.parse(
            localStorage.getItem("productsInBasket")
          ),
        })
      : localStorage.setItem("productsInBasket", JSON.stringify([]));
  }

  updateFavorite = () => {
    this.setState({
      productsInFavorite: JSON.parse(
        localStorage.getItem("productsInFavorite")
      ),
    });
  };

  updateBasket = () => {
    this.setState({
      productsInBasket: JSON.parse(localStorage.getItem("productsInBasket")),
    });
  };

  openModal = (modalId, submitFunction) => {
    this.setState({
      modal: {
        visible: true,
        modalId,
        submitFunction,
      },
    });
  };

  closeModal = () => {
    this.setState({
      modal: {
        visible: false,
        modalId: null,
        submitFunction: null,
      },
    });
  };

  render() {
    return (
      <div className={styles.App}>
        <div className={styles.productCounts}>
          <div
            className={styles.favoriteIcon}
            onClick={() => console.log(this.state.productsInFavorite)}
          >
            <img src={favoriteIcon} alt="" />
            <span className={styles.productCount}>
              {this.state.productsInFavorite.length}
            </span>
          </div>
          <div
            className={styles.favoriteIcon}
            onClick={() => console.log(this.state.productsInBasket)}
          >
            <img src={basketIcon} alt="" />
            <span className={styles.productCount}>
              {this.state.productsInBasket.length}
            </span>
          </div>
        </div>
        <ProductList
          products={this.state.products}
          updateFavorite={this.updateFavorite}
          updateBasket={this.updateBasket}
          openModal={this.openModal}
        />
        {this.state.modal.visible && (
          <Modal
            visible={this.state.modal.visible}
            modalId={this.state.modal.modalId}
            submitFunction={this.state.modal.submitFunction}
            closeModal={this.closeModal}
          />
        )}
      </div>
    );
  }
}


export default App;
