import { Component } from "react";
import PropTypes from "prop-types";

import styles from "../../styles/product.module.scss";
import { Button } from "../button";

import favoriteStar from "../../img/favourites-star.png";
import favoriteStarBlack from "../../img/favourites-star-black.png";

export default class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inBasket: false,
      inFavorite: false,
      product: {},
    };
  }

  componentDidMount() {
    this.setState({ product: this.props.product });

    // перевірка продукту на обране
    const productsInFavoriteArray = JSON.parse(
      localStorage.getItem("productsInFavorite")
    );
    const productInFavorite = productsInFavoriteArray.find(
      (product) => product.article === this.props.product.article
    );
    productInFavorite && this.setState({ inFavorite: true });

    // перевірка продукту чи в корзині
    const productsInBasketArray = JSON.parse(
      localStorage.getItem("productsInBasket")
    );
    const productInBasket = productsInBasketArray.find(
      (product) => product.article === this.props.product.article
    );
    productInBasket && this.setState({ inBasket: true });
  }

  addToFavorite = () => {
    const productsInFavoriteArray = JSON.parse(
      localStorage.getItem("productsInFavorite")
    );
    const productInFavorite = productsInFavoriteArray.find(
      (product) => product.article === this.state.product.article
    );
    if (productInFavorite) return false;
    localStorage.setItem(
      "productsInFavorite",
      JSON.stringify([...productsInFavoriteArray, this.state.product])
    );
    this.setState({ inFavorite: true });
    this.props.updateFavorite();
  };

  removeFromFavorite = () => {
    const productsInFavoriteArray = JSON.parse(
      localStorage.getItem("productsInFavorite")
    );
    const removedProductIndex = productsInFavoriteArray.findIndex(
      (product) => product.article === this.state.product.article
    );
    const updatedproductsInFavoriteArray = [
      ...productsInFavoriteArray.slice(0, removedProductIndex),
      ...productsInFavoriteArray.slice(removedProductIndex + 1),
    ];
    localStorage.setItem(
      "productsInFavorite",
      JSON.stringify(updatedproductsInFavoriteArray)
    );
    this.setState({ inFavorite: false });
    this.props.updateFavorite();
  };

  toggleFavorite = () => {
    if (this.state.inFavorite) this.removeFromFavorite();
    else this.addToFavorite();
  };

  addToBasket = () => {
    const productsInBasketArray = JSON.parse(
      localStorage.getItem("productsInBasket")
    );
    const productInBasket = productsInBasketArray.find(
      (product) => product.article === this.state.product.article
    );
    if (productInBasket) return false;
    localStorage.setItem(
      "productsInBasket",
      JSON.stringify([...productsInBasketArray, this.state.product])
    );
    this.setState({ inBasket: true });
    this.props.updateBasket();
  };

  render() {
    const { title, price, imgUrl, company } = this.state.product;
    return (
      <div className={styles.Product}>
        <div className={styles.Favorite} onClick={this.toggleFavorite}>
          <img
            src={this.state.inFavorite ? favoriteStarBlack : favoriteStar}
            alt=""
          />
        </div>
        <div className={styles.imageWrapper}>
          <img src={imgUrl} alt="" />
        </div>
        <div>
          <h2>{title}</h2>
          <h4>Марка: {company}</h4>
          <h3>Ціна: {price} грн</h3>
        </div>
        <Button
          text={this.state.inBasket ? "Додано в кошик" : "До кошика"}
          backgroundColor={this.state.inBasket ? "grey" : "orange"}
          onClick={() => {
            !this.state.inBasket &&
              this.props.openModal("addToBasket", this.addToBasket);
          }}
        />
      </div>
    );
  }
}

Product.propTypes = {
  product: PropTypes.object,
  updateFavorite: PropTypes.func,
  updateBasket: PropTypes.func,
  openModal: PropTypes.func,
};
