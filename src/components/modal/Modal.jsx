import { Component } from "react";
import PropTypes from "prop-types";

import { Button } from "../button";
import modalSettings from "./modalSetting";
import styles from "../../styles/modal.module.scss";

class Modal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      settings: {},
    };
  }

  componentDidMount() {
    const modal = modalSettings.find(
      (item) => item.modalId === this.props.modalId
    );
    this.setState({ settings: modal.settings });
  }

  render() {
    const { visible, closeModal} = this.props;
    const { header, closeButton, text, actions } = this.state.settings;
    return visible && (
      <div
        className={styles.modalWrapper}
        onClick={closeModal}
      >
        <div className={styles.modal}>
          <div className={styles.modalHeader}>
            {header ? <h2>{header}</h2> : null}
            {closeButton && (
              <Button
                text="X"
                backgroundColor="rgb(163, 35, 0)"
                onClick={closeModal}
              />
            )}
          </div>
          <div className={styles.modalContent}>{text}</div>
          <div className={styles.modalFooter}>
            {actions &&
                  actions.map((item, index) => (
                    <Button
                      text={item.text}
                      onClick={
                        item.type === "submit"
                          ? () => {
                              this.props.submitFunction();
                              this.props.closeModal();
                            }
                          : this.props.closeModal
                      }
                      key={Date.now() + index}
                    />
                  ))}
          </div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  visible: PropTypes.bool,
  modalId: PropTypes.string,
  submit: PropTypes.func,
  closeModal: PropTypes.func,
};
  
export default Modal;
