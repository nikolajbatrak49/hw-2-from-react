const modalSettings = [
  {
    modalId: "addToBasket",
    settings: {
      header: "Придбання товару",
      closeButton: true,
      text: "Бажаєте додати даний товар до кошика?",
      actions: [
        {
          type: "submit",
          backgroundColor: "pink",
          text: "Так!",
        },
        {
          type: "cancel",
          backgroundColor: "red",
          text: "Ні, додам пізніше",
        },
      ],
    },
  },
];

export default modalSettings;
