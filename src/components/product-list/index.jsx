import React, { Component } from "react";
import PropTypes from "prop-types";

import styles from "../../styles/productList.module.scss";
import Product from "../product-item";

export default class ProductList extends Component {
  render() {
    return (
      <div className={styles.ProductList}>
        {this.props.products.map((product) => (
          <Product
            product={product}
            updateFavorite={this.props.updateFavorite}
            updateBasket={this.props.updateBasket}
            openModal={this.props.openModal}
            key={product.article}
          />
        ))}
      </div>
    );
  }
}

ProductList.propTypes = {
  products: PropTypes.array,
  updateFavorite: PropTypes.func,
  updateBasket: PropTypes.func,
  openModal: PropTypes.func,
};
